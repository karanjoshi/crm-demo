package CRM.houzquest;

import org.testng.annotations.Test;
import org.testng.annotations.Test;
import java.io.IOException;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import pageObjects.Admin;
import pageObjects.Calender;
import pageObjects.HQ;
import pageObjects.HomePage;
import pageObjects.LandingPage;
import pageObjects.Mail;
import pageObjects.Reporting;
import pageObjects.Tasks;
import pageObjects.Text;
import resources.Read_XLS;
import resources.SuiteUtility;
import resources.base;

public class HomeScreen extends base{
	Read_XLS FilePath = null;	
	String TestCaseName = null;
	public WebDriver driver;
//	 public static Logger log =LogManager.getLogger(base.class.getName());
	@BeforeTest
	public void initialize() throws IOException
	{
	
		 driver =initializeDriver();
//		 log.info("Driver is initialized");
		 init();	
			FilePath = TestCaseListExcelOne;
			//System.out.println("FilePath Is : "+FilePath);
			TestCaseName = this.getClass().getSimpleName();	
			//System.out.println("TestCaseName Is : "+TestCaseName);
	}	

//	@Test
//	
//	public void searchUser() throws IOException
//	{
//		HomePage lg = new HomePage(driver);
//		lg.search().sendKeys("andre1 fletcher");
//	
//		
//		}
	int i =0;
	@Test(dataProvider="SuiteOneCaseOneData")
	public void ValidatingLead(String name, String mobile, String email) throws InterruptedException {
		//System.out.println(name+ "," + mobile + "," + email);
		HQScheduleTour(name, mobile, email);
//		String l = driver.findElement(By.xpath("/html/body/div[4]/div/div[2]/div/div[2]/div[2]/div[2]/p[1]")).getText();
//		System.out.println(l);

		
		HQ h = new HQ(driver);

		 if((isTextPresent())) {
			 
			 System.out.println(h.tourScheduleSuccess().getText());
		 }else {
				WebDriverWait wait = new WebDriverWait(driver,30);
				wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("/html/body/div[5]/div/span/div/div/div/div[1]")));
			

				
				Thread.sleep(1000);
				String Error = h.Error().getText();
				
				System.out.println(Error);
			 wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("/html/body/div[5]/div/span/div/div/div/div[1]")));
			 Thread.sleep(2000);
			 
			 
		 }

		 
		 
		
//		if(i==0) {
//			login();
//		}else {
//			driver.get(prop.getProperty("CRMurl"));
//		}
//		i++;
//		
//		LandingPage lp = new LandingPage(driver);
//        List<WebElement> colsWithData = lp.tableSecondCol();    
//
//
//        for(int index =0; index<colsWithData.size(); index++) {
//         String splt[] = colsWithData.get(index).getText().split("\\n");
////         System.out.println("Name :"+ splt[0]);
//         if (splt[0].equalsIgnoreCase(name)) {
//        	 System.out.println("Test case passed" + splt[0]);
////             System.out.println("leadSource :"+ splt[1]);
//        	 
//         } else {
//        	 System.out.println("Test case failed");
//         }

//        }

	}
	
	@DataProvider
	public Object[][] SuiteOneCaseOneData(){
		//To retrieve data from Data 1 Column,Data 2 Column,Data 3 Column and Expected Result column of SuiteOneCaseOne data Sheet.
		//Last two columns (DataToRun and Pass/Fail/Skip) are Ignored programatically when reading test data.
		return SuiteUtility.GetTestDataUtility(FilePath, TestCaseName);
	}
	@AfterTest
	public void teardown()
	{
		
		//driver.close();
	
		
	}
}
