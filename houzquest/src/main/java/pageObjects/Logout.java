package pageObjects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import resources.base;

public class Logout extends base{
public WebDriver driver;
	
	
	By profileIcon=By.xpath("//*[@id=\"root\"]/div/section/section/header/div/div/div[2]/div/div/div[4]/span");
	By signout=By.xpath("/html/body/div[10]/div/div/ul/li[7]/span");


	
	
	
	
	
	public Logout(WebDriver driver) {
		// TODO Auto-generated constructor stub
		
		this.driver=driver;
		
	}


	




	public WebElement profileIcon()
	{
		return driver.findElement(profileIcon);
	}
	

	public WebElement signout()
	{
		return driver.findElement(signout);
	}
	


}
