package pageObjects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class AddUser {
	public WebDriver driver;
	By firstName=By.xpath("//*[@id=\"first_name\"]");
	By lastName=By.xpath("/html/body/div[10]/div/div[2]/div/div[2]/div[2]/div/form/div/div[1]/div[2]/div/div[2]/div/span/input");
	By emailAddress=By.xpath("//*[@id=\"email\"]");
	By phoneNumber=By.xpath("//*[@id=\"phone\"]");
	By sourceName=By.xpath("/html/body/div[10]/div/div[2]/div/div[2]/div[2]/div/form/div/div[3]/div[1]/div/div[2]");
	By buyer=By.xpath("//*[@id=\"role\"]/div[1]/label/span[1]/input");
	By seller=By.xpath("//*[@id=\"role\"]/div[2]/label/span[1]/input");
	By city=By.xpath("/html/body/div[10]/div/div[2]/div/div[2]/div[2]/div/form/div/div[4]/div[1]/div/div[2]/div/span/div/div/div");
	By citySelection=By.xpath("/html/body/div[10]/div/div[2]/div/div[2]/div[2]/div/form/div/div[4]/div[1]/div/div[2]/div/span/div/div/div/ul/li/div/input");
	By bedroomSelection=By.xpath("/html/body/div[12]/div/div/div/ul/li[1]");
	
	By price=By.xpath("//*[@id=\"price\"]");
	By bedroom=By.xpath("//*[@id=\"bedroom\"]/div/div");
	By bathroom=By.xpath("/html/body/div[10]/div/div[2]/div/div[2]/div[2]/div/form/div/div[5]/div[2]/div/div[2]/div/span/div/div/div");
	By bathroomSelection=By.xpath("/html/body/div[13]/div/div/div/ul/li[1]");
	By cancel=By.xpath("/html/body/div[10]/div/div[2]/div/div[2]/div[3]/div/button[1]");
	By add=By.xpath("/html/body/div[10]/div/div[2]/div/div[2]/div[3]/div/button[2]");
	By source=By.xpath("/html/body/div[11]/div/div/div/ul/li[3]");
	By notification=By.xpath("//div[@class='ant-notification-notice-message']");
	
	
	
	
	

	
	public AddUser(WebDriver driver) {
		// TODO Auto-generated constructor stub
		
		this.driver=driver;
		
	}
	public WebElement firstName() {
		return driver.findElement(firstName);
		
	}
	public WebElement lastName() {
		return driver.findElement(lastName);
		
	}

public WebElement emailAddress() {
	return driver.findElement(emailAddress);
	
}

public WebElement phoneNumber() {
	return driver.findElement(phoneNumber);
	
}

public WebElement sourceName() {
	return driver.findElement(sourceName);
	
}

public WebElement buyer() {
	return driver.findElement(buyer);
	
}

public WebElement seller() {
	return driver.findElement(seller);
	
}

public WebElement city() {
	return driver.findElement(city);
	
}

public WebElement price() {
	return driver.findElement(price);
	
}

public WebElement bedroom() {
	return driver.findElement(bedroom);
	
}

public WebElement bathroom() {
	return driver.findElement(bathroom);
	
}

public WebElement cancel() {
	return driver.findElement(cancel);
	
}

public WebElement add() {
	return driver.findElement(add);
	
}

public WebElement source() {
	return driver.findElement(source);
	
}
public WebElement citySelection() {
	return driver.findElement(citySelection);
	
}


public WebElement bedroomSelection() {
	return driver.findElement(bedroomSelection);
	
}

public WebElement bathroomSelection() {
	return driver.findElement(bathroomSelection);
	
}

public WebElement notification() {
	return driver.findElement(notification);
	
}



}

