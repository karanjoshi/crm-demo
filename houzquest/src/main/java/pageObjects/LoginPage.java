package pageObjects;

import java.io.IOException;
import java.util.Iterator;
import java.util.Set;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.BeforeTest;

import resources.base;

public class LoginPage extends base{

public WebDriver driver;
	
	
	By email=By.xpath("//*[@id=\"identifierId\"]");
	By password=By.xpath("//*[@id=\"password\"]/div[1]/div/div[1]/input");
	By usrNext=By.xpath("//*[@id=\"identifierNext\"]/div/button/div[2]");
	By login=By.xpath("//*[@id=\"passwordNext\"]/div/button/div[2]");

	
	
	
	
	
	public LoginPage(WebDriver driver) {
		// TODO Auto-generated constructor stub
		
		this.driver=driver;
		
	}


	




	public WebElement getEmail()
	{
		return driver.findElement(email);
	}
	

	public WebElement getPassword()
	{
		return driver.findElement(password);
	}
	
	public WebElement next()
	{
		return driver.findElement(usrNext);
	}
	
	public WebElement getLogin()
	{
		return driver.findElement(login);
	}

}
