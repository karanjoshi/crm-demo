package pageObjects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class HQ {
	public WebDriver driver;
	By Oakland=By.xpath("//*[@id=\"root\"]/div/div/section/main/div[2]/div/div/div/div[2]/div[7]/div");
	By property=By.xpath("//*[@id=\"root\"]/div/div/section/main/div/div[2]/div[2]/div/div/div[1]/div[2]/div/div/div/div[2]/div/div");
	By scheduleTour=By.cssSelector("button[class*='ant-btn text-medium shadow ant-btn-secondary']");
	By name=By.xpath("//*[@id=\"fullname\"]");
	
	By mobile=By.xpath("//*[@id=\"phone\"]");
	By email=By.xpath("//*[@id=\"email\"]");
	By date=By.xpath("//div/div[2]/div/div[2]/div[2]/form/div/div/div/div[1]/div/div/div/div/div/div[1]");
	By time=By.xpath("//div/div[2]/div/div[2]/div[2]/form/div/div/div/div[1]/div[2]/div/div/div/div/div[1]/div/div/div/label/span[2]/p");
	By submit=By.xpath("//div/div[2]/div/div[2]/div[2]/form/div/div/div/div[3]/div[4]/div/div/span/button");
	By Error=By.xpath("/html/body/div[5]/div/span/div/div/div/div[1]");
	By tourScheduleSuccess=By.xpath("/html/body/div[4]/div/div[2]/div/div[2]/div[2]/div[2]/p[1]");
	
	
	
	
	
	
	
	
	
	public HQ(WebDriver driver) {
		// TODO Auto-generated constructor stub
		
		this.driver=driver;
		
	}
	public WebElement Oakland() {
		return driver.findElement(Oakland);
		
	}
	
	public WebElement property() {
		return driver.findElement(property);
		
	}
	
	public WebElement scheduleTour() {
		return driver.findElement(scheduleTour);
		
	}
	public WebElement name() {
		return driver.findElement(name);
		
	}
	public WebElement mobile() {
		return driver.findElement(mobile);
		
	}
	public WebElement email() {
		return driver.findElement(email);
		
	}
	public WebElement date() {
		return driver.findElement(date);
		
	}
	public WebElement time() {
		return driver.findElement(time);
		
	}
	public WebElement submit() {
		return driver.findElement(submit);
		
	}
	public WebElement Error() {
		return driver.findElement(Error);
		
	}
	public WebElement tourScheduleSuccess() {
		return driver.findElement(tourScheduleSuccess);
		
	}
}
