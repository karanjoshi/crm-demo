package pageObjects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class Mail {
	public WebDriver driver;
	By emailContent=By.xpath("//*[@id=\"root\"]/div/section/section/main/div/div/div[2]/div[2]/div/div/div/div[1]/div[2]/div/div/div/div/div/div/ul/div[1]/div[1]");
	
	public Mail(WebDriver driver) {
		// TODO Auto-generated constructor stub
		
		this.driver=driver;
		
	}
	public WebElement content() {
		return driver.findElement(emailContent);
		
	}
}

