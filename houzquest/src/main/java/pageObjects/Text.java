package pageObjects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class Text{
	public WebDriver driver;
	By newConversation=By.xpath("//*[@id=\"root\"]/div/section/section/main/div/div/div[1]/div/div/div/li/button");
	
	public Text(WebDriver driver) {
		// TODO Auto-generated constructor stub
		
		this.driver=driver;
		
	}
	public WebElement content() {
		return driver.findElement(newConversation);
		
	}
}
