package resources;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import java.util.Properties;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.NoAlertPresentException;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import pageObjects.HQ;
//import CRM.houzquest.Login;
import pageObjects.LandingPage;
import pageObjects.LoginPage;
import pageObjects.Logout;

public class base {

	public  static WebDriver driver;
	public static Properties prop;
	protected static String childId;
	protected static String parentId;
	
	protected static ArrayList <String> srtingArray;
	
	public static Read_XLS TestSuiteListExcel=null;
	public static Read_XLS TestCaseListExcelOne=null;
	public static Read_XLS TestCaseListExcelTwo=null;
	ChromeOptions options = new ChromeOptions();
	
	public void init() throws IOException{
		//Please change file's path strings bellow If you have stored them at location other than bellow.
		//Initializing Test Suite List(TestSuiteList.xls) File Path Using Constructor Of Read_XLS Utility Class.
		TestSuiteListExcel = new Read_XLS(System.getProperty("/home/wmt-qa/eclipse-workspace/crm-demo.git/houzquest/src/main/java/excelFiles/SuiteOne.xls"));
		//Initializing Test Suite One(SuiteOne.xls) File Path Using Constructor Of Read_XLS Utility Class.
		TestCaseListExcelOne = new Read_XLS(System.getProperty("/home/wmt-qa/eclipse-workspace/crm-demo.git/houzquest/src/main/java/excelFiles/SuiteTwo.xls"));
		//Initializing Test Suite Two(SuiteTwo.xls) File Path Using Constructor Of Read_XLS Utility Class.
		TestCaseListExcelTwo = new Read_XLS(System.getProperty("/home/wmt-qa/eclipse-workspace/crm-demo.git/houzquest/src/main/java/excelFiles/TestSuiteList.xls"));																			
	}
public WebDriver initializeDriver() throws IOException
{

//	ChromeOptions options = new ChromeOptions();
    Map<String, Object> prefs = new HashMap<String, Object>();
    Map<String, Object> profile = new HashMap<String, Object>();
    Map<String, Object> contentSettings = new HashMap<String, Object>();

    // SET CHROME OPTIONS	
    // 0 - Default, 1 - Allow, 2 - Block
    contentSettings.put("notifications", 1);
    profile.put("managed_default_content_settings", contentSettings);
    prefs.put("profile", profile);
    options.setExperimentalOption("prefs", prefs);
 prop= new Properties();
FileInputStream fis=new FileInputStream("/home/wmt-qa/eclipse-workspace/crm-demo.git/houzquest/src/main/java/resources/data.properties");
//("user.dir")+"\\reports\\index.html";

prop.load(fis);
String browserName=prop.getProperty("browser");
System.out.println(browserName);


if(browserName.equals("chrome"))
{
	 System.setProperty("webdriver.chrome.driver", "chromedriver");
	driver= new ChromeDriver(options);
	driver.manage().window().maximize();
		//execute in chrome driver
	
}
else if (browserName.equals("firefox"))
{
	 driver= new FirefoxDriver();
	//firefox code
}
else if (browserName.equals("IE"))
{
//	IE code
}

driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
return driver;


}

public  void windowHandler(WebDriver driver) {
	Set<String> windows = driver.getWindowHandles(); //[parentid,childid,subchildId]
	Iterator<String>it = windows.iterator();
	int s = driver.getWindowHandles().size();
//	System.out.println(s);
	srtingArray = new ArrayList<String>();
	for(int i = 0; i<s; i++) {
		  srtingArray.add(it.next());

		
	}
	System.out.println(srtingArray.get(0));

	
//	 this.parentId = it.next();
//
//
//	this.childId = it.next();
	
	
}
public String getScreenShotPath(String testCaseName,WebDriver driver) throws IOException
{
	TakesScreenshot ts=(TakesScreenshot) driver;
	File source =ts.getScreenshotAs(OutputType.FILE);
	String destinationFile = System.getProperty("user.dir")+"/reports/"+testCaseName+".png";
	FileUtils.copyFile(source,new File(destinationFile));
	return destinationFile;



}

public  void login() throws InterruptedException {
	
	
	driver.get(prop.getProperty("CRMurl"));
	LandingPage l=new LandingPage(driver);
	Thread.sleep(2000);
	LoginPage lp=l.getLogin(); //driver.findElement(By.css()
	windowHandler(driver);
	
	//Login lo = new Login();
Thread.sleep(2000);
	driver.switchTo().window(srtingArray.get(1));

	lp.getEmail().sendKeys("test2@webmob.tech");
	lp.next().click();
	lp.getPassword().sendKeys("WMTTest1@7");
	

	//log.info(text);
	
	lp.getLogin().click();
	Thread.sleep(10000);
	driver.switchTo().window(srtingArray.get(0));
}

public void HQScheduleTour(String name, String mobile, String email) throws InterruptedException,  ArrayIndexOutOfBoundsException {
	 driver.get(prop.getProperty("HQurl"));
//	 System.out.println(name+ "," + mobile + "," + email);
		 HQ hq = new HQ(driver);
		 

		 hq.Oakland().click();
		 hq.property().click();
		 WebDriverWait wait = new WebDriverWait(driver,30);
		 wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector("button[class*='ant-btn text-medium shadow ant-btn-secondary']"))).click();
//		 hq.scheduleTour().click();
		 hq.name().sendKeys(name);
		 hq.mobile().sendKeys(mobile);
		 hq.email().sendKeys(email);
//		 wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("/html/body/div[3]/div/div[2]/div/div[2]/div[2]/form/div/div/div/div[1]/div/div/div/div/div/div[1]"))).click();

		 hq.date().click();
		 hq.time().click();
		 Thread.sleep(2000);
		 hq.submit().click();
		 
			wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//div[contains(@class,'ant-notification-notice-message')]")));
		



			try {
			    List<WebElement> listOfElements = driver.findElements(By.xpath("//div[contains(@class,'ant-notification-notice-message')]"));
				Thread.sleep(5000);
				listOfElements.size();
				for(int i =0; i<=listOfElements.size();i++) {
					System.out.println(listOfElements.get(i).getText());
					
				}
			}
			catch(org.openqa.selenium.StaleElementReferenceException ex)
			{
			    List<WebElement> listOfElements = driver.findElements(By.xpath("//div[contains(@class,'ant-notification-notice-message')]"));
				Thread.sleep(5000);
				listOfElements.size();
				for(int i =0; i<=listOfElements.size();i++) {
					System.out.println(listOfElements.get(i).getText());
					
				}
			}
//			String Error = hq.Error().getText();
//			
//			System.out.println(Error);

}

public  void logout() throws InterruptedException {
	Logout lo = new Logout(driver);
	lo.profileIcon().click();
	lo.signout().click();
}

public void clearCache() throws InterruptedException {
    //ChromeOptions chromeOptions = new ChromeOptions();
	options.addArguments("disable-infobars");
	options.addArguments("start-maximized");
    //driver = new ChromeDriver(options);
	
    driver.get("chrome://settings/clearBrowserData");
    Thread.sleep(10000);
    driver.switchTo().activeElement();
    driver.findElement(By.xpath("//*[@id=\"clearBrowsingDataDialog\"]")).click();;
//    if(l.equals(driver.switchTo().activeElement())) {
//    	System.out.println("Focus is on dailogue box");
//    
//    }
//
//     else {
//         System.out.println("Element is not focused");
//         driver.close();
//     }

    driver.findElement(By.xpath("//*[@id=\"clearBrowsingDataConfirm\"]")).click();
    System.out.println("executed once");
    Thread.sleep(5000);
}

public void ClearBrowserCache() throws InterruptedException
{
driver.manage().deleteAllCookies(); //delete all cookies
Thread.sleep(7000); //wait 7 seconds to clear cookies.
}

public boolean isAlertPresent() 
{ 
    try 
    { 
        driver.switchTo().alert(); 
        return true; 
    }   // try 
    catch (NoAlertPresentException Ex) 
    { 
        return false; 
    }   // catch 
}

public boolean isTextPresent() 
{ 
    try 
    { 
    	driver.findElement(By.xpath("/html/body/div[4]/div/div[2]/div/div[2]/div[2]/div[2]/p[1]")); 
        return true; 
    }   // try 
    catch (NoSuchElementException Ex) 
    { 
        return false; 
    }   // catch 
}

}
